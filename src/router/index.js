import Vue from 'vue'
import Router from 'vue-router'

import Main from '../pages/main'
import News from '../pages/news'
import NewDetail from '../pages/newdetail'
import Photos from '../pages/photos'
import PhotoDetail from '../pages/photodetail'

Vue.use(Router)

export default new Router({
  mode: 'history',
  linkActiveClass:'active',
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      name:'news',
      path:'/news',
      component:News
    },
    {
      name:'detail',
      path:'/news/detail',
      component:NewDetail
    },
    {
      name:'photos',
      path:'/photos/:categoryId',
      component:Photos
    },
    {
      name:'photodetail',
      path:'/photos/detail',
      component:PhotoDetail
    }
  ]
})
