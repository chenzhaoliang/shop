// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import './css/common.css'

//引入mint-ui全部的组件
import Mint from 'mint-ui'
Vue.use(Mint);
import 'mint-ui/lib/style.css'

//配置网络请求的axios
import axios from 'axios'
//Axios.defaults.baseURL = 'https://www.baidu.com'
Vue.prototype.$axios = axios

//注册全局组件
import Navbar from './common/Navbar'
Vue.component(Navbar.name,Navbar)

//全局引入事件处理的类
import Moment from 'moment'
Vue.filter('convertTime', function (date, formatStr) {
  return Moment(date).format(formatStr)
})

//添加图片预览组件
import VuePreview from 'vue-preview'
Vue.use(VuePreview)

Vue.config.productionTip = false

//字数控制
Vue.filter('controlShow',function (str, num) {
  if (str.length <= num)
    return str
  else
    return str.substr(0, num-1)+'...'
})

//添加请求拦截器
axios.interceptors.request.use(function (config) {
  //加载提示框
  Mint.Indicator.open({
    text:'玩命加载中'
  })
  return config
}, function (error) {
  Mint.Indicator.close()
  return Promise.reject(error)
})

//添加相应拦截器
axios.interceptors.response.use(function (config) {
  Mint.Indicator.close()
  return config
}, function (error) {
  Mint.Indicator.close()
  return Promise.reject(error)
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
